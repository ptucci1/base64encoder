﻿using System.IO;
using System.Threading.Tasks;

namespace Base64EncoderLibrary
{
    public class FileDecoder
    {
        public Task DecodeAsync(string path)
        {
            return DecodeAsync(new FileInfo(path));
        }

        public async Task DecodeAsync(FileInfo file)
        {
            var decoder = new StreamingDecoder(file.FullName);
            var originalFile = new FileInfo(await decoder.ReadLineAsync());
            var outputFilePath = Path.Combine(file.Directory!.FullName, originalFile.Name);
            using var writer = new FileStream(outputFilePath, FileMode.CreateNew, FileAccess.Write);
            
            while(!decoder.EndOfStream)
            {
                await writer.WriteAsync(await decoder.ReadLineAsBytesAsync());
            }
        }
    }
}

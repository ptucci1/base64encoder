﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Base64EncoderLibrary
{
    public class StreamingEncoder
    {
        public int BufferSize { get; }

        public StreamingEncoder(int bufferSize = 4096)
        {
            BufferSize = bufferSize;
        }

        public IEnumerable<string> Encode(string inputPath)
        {
            long read = 0;
            var buffer = new byte[BufferSize];
            using var reader = new FileStream(inputPath, FileMode.Open, FileAccess.Read);

            var currentRead = 0;
            while ((currentRead = reader.Read(buffer, 0, BufferSize)) > 0)
            {
                read += currentRead;
                if (currentRead < BufferSize)
                {
                    yield return Convert.ToBase64String(buffer.Take(currentRead).ToArray());
                }
                else
                {
                    yield return Convert.ToBase64String(buffer);
                }
            }
        }

        public async IAsyncEnumerable<string> EncodeAsync(string inputPath)
        {
            long read = 0;
            var buffer = new byte[BufferSize];
            using var reader = new FileStream(inputPath, FileMode.Open, FileAccess.Read);

            var currentRead = 0;
            while ((currentRead = await reader.ReadAsync(buffer, 0, BufferSize)) > 0)
            {
                read += currentRead;
                if (currentRead < BufferSize)
                {
                    yield return Convert.ToBase64String(buffer.Take(currentRead).ToArray());
                }
                else
                {
                    yield return Convert.ToBase64String(buffer);
                }
            }
        }
    }
}